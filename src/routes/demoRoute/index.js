import express from 'express';
import { demoRouteController } from '../../controllers';

const demoRoute = express.Router();

demoRoute.get('/:id', (req, res, next) => {
  demoRouteController(req.params.id)
    .then((result) => {
      res.send(result);
    }).catch((err) => {
      next(err);
    });
});

export default demoRoute;

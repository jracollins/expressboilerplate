import express from 'express';
import logger from './logger';
import router from './router';
import { logErrors, clientErrorHandler, catchAllErrorHandler } from './errorHandlers';
require('nodejs-dashboard');

const app = express();

// Winston logging middleware
app.use(logger);

// All routes
app.use(router);

// Error catching
app.use(logErrors);
app.use(clientErrorHandler);
app.use(catchAllErrorHandler);

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});

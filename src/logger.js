import expressWinston from 'express-winston';
import winston from 'winston';
import 'winston-logstash';

export default expressWinston.logger({
  transports: [
    new winston.transports.Console({
      json: false,
      colorize: true
    }),
    new winston.transports.File({
      json: true,
      filename: 'requests.log'
    }).on('error', (err) => {
      console.error(err);
    }),
    // new winston.transports.Logstash({
    //   port: 5000,
    //   node_name: 'logstash',
    //   host: '127.0.0.1'
    // }).on('error', (err) => {
    //   console.error(err);
    // })
  ],
  meta: true,
  msg: 'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}',
  expressFormat: true,
  colorize: false
});

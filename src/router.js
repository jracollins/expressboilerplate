import express from 'express';
import { route } from './routes';

const router = express.Router();
router.use('/route', route);

export default router;

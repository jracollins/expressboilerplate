const gulp = require('gulp');
const babel = require('gulp-babel');
const nodemon = require('gulp-nodemon');
const FileCache = require('gulp-file-cache');
const del = require('del');
const eslint = require('gulp-eslint');

const cache = new FileCache();

gulp.task('build', function () {
  const stream = gulp.src(['./src/**/*.js', '!**/__tests__/**'])
  .pipe(cache.filter())
  .pipe(babel())
  .pipe(cache.cache())
  .pipe(gulp.dest('./dist'));
  return stream;
});

gulp.task('clean', function () {
  return del(['dist', '.gulp-cache']);
});

gulp.task('clean-logs', function () {
  return del(['requests.log']);
});

gulp.task('lint', function () {
  return gulp.src(['./src/**/*.js', '!node_modules/**'])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('dev', ['build'], function () {
  nodemon({
    script: './dist/app.js',
    ext: 'js',
    ignore: [
      'node_modules/',
      '__test__'
    ],
    watch: 'src',
    tasks: ['build'],
    env: { 'NODE_ENV': 'development' }
  })
  .on('restart', function () {
    console.log('nodemon restarted server');
  });
});
